<?php
$connection = mysqli_connect("localhost","root","","locator");
mysqli_set_charset($connection,"utf8");
$query = "SELECT 
	cities.id AS 'id', 
	cities.name AS 'city', 
	countries.name AS 'country',
	countries.short_name AS 'short_name',
	cities.latitude AS 'latitude',
	cities.longitude AS 'longitude'
FROM cities
LEFT JOIN countries
	ON cities.country=countries.id;";
$result = mysqli_query($connection,$query);
?>
<table class="table">
  <thead>
    <tr>
      <th scope="col">City</th>
      <th scope="col">Country</th>
      <th scope="col">Latitude</th>
      <th scope="col">Longitude</th>
      <th scope="col">Link</th>
    </tr>
  </thead>
<?php while($city = mysqli_fetch_assoc($result)) { ?>
<tr>
<th scope="col"><?php echo $city["city"] ?></th>
<th scope="col"><?php echo $city["country"]." (".$city["short_name"].")"; ?></th>
<th scope="col"><?php echo $city["latitude"] ?></th>
<th scope="col"><?php echo $city["longitude"] ?></th>
<th scope="col"><a target="_blank" href="https://www.google.ro/maps/@<?php echo $city["latitude"]?>,<?php echo $city["longitude"]?>,10z">See on Google Maps</a></th>
</tr>
<?php } ?>
</table>