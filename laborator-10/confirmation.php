<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$city = trim($_POST["city"]);
	$link = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($city);
	$content = file_get_contents($link);
	$content = json_decode($content,TRUE);
	if($content["status"]=="OK"){
		$city_name = "";
		$country_name = "";
		$country_short_name = "";
		$content = reset($content["results"]);
		foreach($content["address_components"] as $component){
			if(in_array("locality",$component["types"])){
				$city_name = $component["long_name"];
			}
			if(in_array("country",$component["types"])){
				$country_name = $component["long_name"];
				$country_short_name = $component["short_name"];
			}
		}
		$formatted_address = $content["formatted_address"];
		$latitude = $content["geometry"]["location"]["lat"];
		$longitude = $content["geometry"]["location"]["lng"];

		$connection = mysqli_connect("localhost","root","","locator");
		mysqli_set_charset($connection,"utf8");

		$query = "SELECT * FROM countries WHERE name LIKE '$country_name';";
		$result = mysqli_query($connection,$query);
		if(mysqli_num_rows($result)>0){
			$temporary = mysqli_fetch_assoc($result);
			$countryID = $temporary["id"];
		} else {
			$query = "INSERT INTO countries (name,short_name) VALUES ('$country_name','$country_short_name');";
			$result = mysqli_query($connection,$query);
			$countryID = mysqli_insert_id($connection);
		}

		$query = "SELECT * FROM cities WHERE formatted_address LIKE '$formatted_address';";
		$result = mysqli_query($connection,$query);
		if(mysqli_num_rows($result)==0){
			$query = "INSERT INTO cities (name,formatted_address, country,latitude,longitude) VALUES ('$city_name','$formatted_address',$countryID,$latitude,$longitude);";
			$result = mysqli_query($connection,$query);
			echo "SUCCESS! City '$formatted_address' was added to the database.";
		} else {
			echo "ERROR! City '$formatted_address' already exists.";
		}
	} else {
		echo "ERROR! Could not connect to Google Maps.";
	}
} else {
	header("Location: index.php");
}
?>
<a href="index.php">Home</a>