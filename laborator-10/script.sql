CREATE DATABASE locator
	DEFAULT CHARACTER SET utf8
	DEFAULT COLLATE utf8_general_ci;

DROP TABLE IF EXISTS cities;
DROP TABLE IF EXISTS countries;

CREATE TABLE countries (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,

	name VARCHAR(255),
	short_name VARCHAR(255),

	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE cities (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,

	name VARCHAR(255),
	formatted_address VARCHAR(255),
	country INT(11),
	latitude FLOAT(3.7),
	longitude FLOAT(3.7),

	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP,

	FOREIGN KEY (country) REFERENCES countries(id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

SELECT 
	cities.id AS 'id', 
	cities.name AS 'city', 
	countries.name AS 'country',
	countries.short_name AS 'short_name',
	cities.latitude AS 'latitude',
	cities.longitude AS 'longitude'
FROM cities
LEFT JOIN countries
	ON cities.country=countries.id;