<?php
$connection = mysqli_connect("localhost","root","","DB");
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$query = "INSERT INTO countries (name,continent) VALUES ('$_POST[name]',$_POST["continent"])";
	$result = mysqli_query($connection,$query);
}

$query = "SELECT * FROM continents";
$result = mysqli_query($connection,$query);
$continents = array();
while($line = mysqli_fetch_assoc($result)){
	$continents[$line["id"]] = $line["name"];
}

mysqli_close($connection);
?>
<form method="POST">
	<input type="text" name="name" placeholder="Country">
	<select name="continent">
		<?php foreach($continents as $id => $continent): ?>
			<option value="<?php echo $id ?>"><?php echo $continent ?></option>
		<?php endforeach; ?> 
	</select>
	<input type="submit">
</form>
