CREATE TABLE continents (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255)
);

CREATE TABLE countries (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255),
	continent INT(11),	
	FOREIGN KEY (continent) REFERENCES continents(id)
);

CREATE TABLE cities (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255),
	country INT(11),	
	FOREIGN KEY (country) REFERENCES countries(id)
);