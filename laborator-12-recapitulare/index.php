<?php

if($_SERVER["REQUEST_METHOD"]=="POST"){
	$file = fopen("answers.txt","a+");
	fputs($file,json_encode($_POST).PHP_EOL);
	fclose($file);
}

$users = array(
	1 => "Marius",
	2 => "Darius",
	3 => "Patricia",
	4 => "Mihai",
	5 => "Marian",
	6 => "George"
);

$grades = array();
$file = fopen("answers.txt","r");
while ($line = fgets($file)){
	$line = json_decode($line,TRUE);
	if(!array_key_exists($line["student"], $grades)){
		$grades[$line["student"]] = array("grade" => intval($line["grade"]), "answers"=> 1);
	} else {
		$grades[$line["student"]]["grade"] += intval($line["grade"]);
		$grades[$line["student"]]["answers"]++;
	}
}
fclose($file);
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<div class="row">

				<table class="table">
				  <thead>
				    <tr>
				      <th scope="col">Student</th>
				      <th scope="col">Grade</th>
				    </tr>
				  </thead>
				  <?php foreach ($grades as $key => $value): ?>
				  	<tr>
				      <td><?php echo $users[$key]; ?></td>
				      <td><?php echo number_format($value["grade"]/$value["answers"],2); ?></td>
				    </tr>
				  <?php endforeach; ?>
				</table>
				<h1>Next Student: <?php $next = rand(1,6); echo $users[$next]; ?></h1>
			</div>
			<form method="POST">
				<div class="form-group">
					<label for="student">Student</label>
					<select name="student" id="student" class="form-control">
						<?php foreach($users as $key => $value): ?>
						<option <?php if($next==$key) echo "selected" ?> value="<?php echo $key ?>"><?php echo $value ?></option>
						<?php endforeach; ?>
					</select> 
				</div>
				<div class="form-group">
					<label for="question">Question</label>
					<input type="text" class="form-control" id="question" name="question">
				</div>
				<div class="form-group">
					<label for="question">Answer</label>
					<textarea class="form-control" id="answer" name="answer"></textarea>
				</div>
				<div class="form-group">
					<label for="grade">Grade</label>
					<input type="text" class="form-control" id="grade" name="grade">
				</div>
				<div class="form-group">
					<label for="type">Type</label>
					<input type="text" class="form-control" id="type" name="type">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	</body>
</html>


FORMULAR POST care te mentine pe pagina 
Ce contine 3 campuri
nume, cnp si email
validati cele 3campuri in post 
nume - required
cnp = required si 13 caractere numerice
email - require is type email