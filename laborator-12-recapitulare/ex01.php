<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$validation = true;
	$fields = array("name","email","cnp");
	foreach($fields as $field){
		if(empty(trim($_POST[$field]))){
			$validation = false;
			echo "Field '$field' is required<br>";
		}
	}
	if(!empty(trim($_POST["cnp"]))){
		if(strlen(trim($_POST["cnp"]))!=13){
			echo "CNP must have 13 characters<br>";
		} elseif (!is_numeric(trim($_POST["cnp"]))){
			echo "CNP must have numeric characters<br>";
		}
	}
	if(!empty(trim($_POST["email"]))){
		if(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $_POST["email"])){
			echo "Email format invalid.";
		}
	}
}
?>
<form method="POST">
	<input type="text" name="name" placeholder="Name">
	<input type="text" name="email" placeholder="Email">
	<input type="text" name="cnp" placeholder="CNP">
	<input type="submit">
</form>