<?php
function generateWord(){
	$vocale = "aeiou";
	$consoane = "bcdfghjklmnprstvxyz";

	$lungimeCuvant = rand(2,10);
	$tip = rand(1,2);
	$cuvant = "";
	for($i=0;$i<$lungimeCuvant;$i++){
		if($tip==1){
			$cuvant .= $vocale[rand(0,strlen($vocale)-1)];
			$tip++;
		} else {
			$cuvant .= $consoane[rand(0,strlen($consoane)-1)];
			$tip--;
		}
	}
	return $cuvant;
}

function generatePhrase(){
	$rand = rand(5,20);
	$arrayCuvinte = array();
	for($i=0;$i<$rand;$i++){
		array_push($arrayCuvinte, generateWord());
	}
	return ucfirst(implode(" ",$arrayCuvinte)).".";
}

function generateParagraph(){
	$rand = rand(5,20);
	$arrayPropozitii = array();
	for($i=0;$i<$rand;$i++){
		array_push($arrayPropozitii, generatePhrase());
	}
	echo ucfirst(implode(" ",$arrayPropozitii)).".";
}

generateParagraph();

generateParagraph();

generateParagraph();

generateParagraph();

generateParagraph();