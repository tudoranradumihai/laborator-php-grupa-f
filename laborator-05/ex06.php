<?php
include "words.php";
// afisati numarul de cuvinte pentru fiecare grup de cuvinte de x litere
/*
ana are mere si ene are carti
|
|-*
|-*
|-****
|-*
|
*/
$grinda = array();
foreach($array as $word){
	$length = strlen($word);
	if(array_key_exists($length, $grinda)){
		$grinda[$length]++;
	} else {
		$grinda[$length] = 1;
	}
}
ksort($grinda);
foreach($grinda as $lungimeCuvant => $numarDeCuvinte){
	echo "Exista $numarDeCuvinte cuvinte de $lungimeCuvant litere<br>";
}