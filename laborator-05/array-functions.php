<?php
// http://php.net/manual/en/ref.array.php
/*
$array = array(1,2,3);
$array = array(
	0 => 1,
	1 => 2,
	2 => 3
);
*/
$array = array(
	"k1"=>"v1",
	"k2"=>"v2"
);

// http://php.net/manual/en/function.array-key-exists.php
$result = array_key_exists("k1", $array);
var_dump($result); 
echo "<br>";
$result = array_key_exists("k3", $array);
var_dump($result); 
echo "<br>";

// http://php.net/manual/en/function.in-array.php
$result = in_array("v1",$array);
var_dump($result); 
echo "<br>";

$array = array(1,2,3,4,5);
// http://php.net/manual/en/function.array-push.php
$result = array_push($array,10);
var_dump($result); 
echo "<br>";
var_dump($array); 
echo "<br>";

$array = array(1,2,3,4,5);
// http://php.net/manual/en/function.array-pop.php
$result = array_pop($array);
var_dump($result); 
echo "<br>";
var_dump($array); 
echo "<br>";

$array = array(
	"firstname" => "John",
	"lastname"  => "Doe"
);
// http://php.net/manual/en/function.array-values.php
$result = array_values($array);
var_dump($result); 
echo "<br>";
var_dump($array); 
echo "<br>";

$array = array(
	"firstname" => "John",
	"lastname"  => "Doe"
);
// http://php.net/manual/en/function.array-keys.php
$result = array_keys($array);
var_dump($result); 
echo "<br>";
var_dump($array); 
echo "<br>";

$keys = array("firstname","lastname");
$values = array("John","Doe");
// http://php.net/manual/en/function.array-combine.php
$result = array_combine($keys, $values);
var_dump($result); 
echo "<br>";

$array = array(1,2,3,4,5,2,4,4);
// http://php.net/manual/en/function.count.php
$result = count($array);
var_dump($result); 
echo "<br>";

// http://php.net/manual/en/function.array-count-values.php
$result = array_count_values($array);
var_dump($result); 
echo "<br>";

$array = array("mere","pere","mere","prune","gogonele");
$result = array_count_values($array);
var_dump($result); 
echo "<br>";

$array1 = array(1,2,3,4,5);
$array2 = array(1,2,6);
// http://php.net/manual/en/function.array-diff.php
$result = array_diff($array1,$array2);
var_dump($result); 
echo "<br>";

$array1 = array(1,2,3,4,5);
$array2 = array(1,2,6);
// http://php.net/manual/en/function.array-merge.php
$result = array_merge($array1,$array2);
var_dump($result); 
echo "<br>";