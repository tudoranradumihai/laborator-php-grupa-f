<?php

// http://php.net/manual/en/control-structures.while.php
$index = 1;
while ($index <= 5){
	echo $index++."<br>";
}

$index = 10;
while ($index <= 5){
	echo $index++."<br>";
}

// http://php.net/manual/en/control-structures.do.while.php
$index = 1;
do {
	echo $index++."<br>";
} while ($index <= 5);

$index = 10;
do {
	echo $index++."<br>";
} while ($index <= 5);

// http://php.net/manual/en/control-structures.for.php
for($i=1;$i<=5;$i++){
	echo $i."<br>";
}
for($i=5;$i>=1;$i--){
	echo $i."<br>";
}

$array = array(1,2,3,4,5);
// http://php.net/manual/en/control-structures.foreach.php
foreach($array as $value){
	echo $value."<br>";
}

$array = array("mere","pere","prune");
foreach($array as $value){
	echo "Ana are ".$value."<br>";
}

$array = array(1,2,3,4,5);
foreach($array as $key => $value){
	echo "valoarea:$value are cheia:$key<br>";
}