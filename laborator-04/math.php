<?php
// http://php.net/manual/ro/ref.math.php

// http://php.net/manual/ro/function.intval.php
$number = 4.23;
echo intval($number);
echo "<br>";

// http://php.net/manual/ro/function.intval.php
$number = 4.99;
echo intval($number);
echo "<br>";

$number = -2; 
var_dump($number);
echo "<br>";

// http://php.net/manual/ro/function.abs.php
$number = abs($number);
var_dump($number);
echo "<br>";

$number = 2.1;
// http://php.net/manual/ro/function.ceil.php
$number = ceil($number);
var_dump($number);
echo "<br>";

$number = 2.9;
// http://php.net/manual/ro/function.floor.php
$number = floor($number);
var_dump($number);
echo "<br>";

$number = 2.9;
// http://php.net/manual/ro/function.round.php
$number = round($number);
var_dump($number);
echo "<br>";

$number = 2.49;
// http://php.net/manual/ro/function.round.php
$number = round($number,1);
var_dump($number);
echo "<br>";

$number = 2.449;
// http://php.net/manual/ro/function.round.php
$number = round($number,2);
var_dump($number);
echo "<br>";

$number = 2.5;
// http://php.net/manual/ro/function.round.php
$number = round($number,0,PHP_ROUND_HALF_DOWN);
var_dump($number);
echo "<br>";

// http://php.net/manual/ro/function.min.php
echo min(1,2,3);
echo "<br>";
$array=array(1,2,3);
echo min($array);
echo "<br>";

// http://php.net/manual/ro/function.max.php
echo max(1,2,3);
echo "<br>";

// http://php.net/manual/ro/function.rand.php
echo rand();
echo "<br>";

// http://php.net/manual/ro/function.rand.php
echo rand(1,10);
echo "<br>";

// http://php.net/manual/ro/function.rand.php
echo rand(1000,9999);
echo "<br>";