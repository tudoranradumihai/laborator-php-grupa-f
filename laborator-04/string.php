<?php
// http://php.net/manual/en/ref.strings.php

$string = "how are you Michael?";
echo $string."<br>";
// http://php.net/manual/en/function.strtoupper.php
echo strtoupper($string)."<br>";
// http://php.net/manual/en/function.strtolower.php
echo strtolower($string)."<br>";
// http://php.net/manual/en/function.ucfirst.php
echo ucfirst($string)."<br>";
// http://php.net/manual/en/function.ucwords.php
echo ucwords($string)."<br>";
// http://php.net/manual/en/function.lcfirst.php
echo lcfirst($string)."<br>";
// http://php.net/manual/en/function.strlen.php
echo strlen($string)."<br>";

$temporary = " How are you!      ";
echo $temporary."<br>";
var_dump($temporary); echo "<br>";
// http://php.net/manual/en/function.trim.php
var_dump(trim($temporary)); echo "<br>";

// http://php.net/manual/en/function.str-replace.php
$string = "Ana are mere!";
echo str_replace("mere","pere",$string)."<br>";

$string = "Michael a zis S*** si f***!";
echo str_replace(array("s***","f***"),"cenzurat",$string)."<br>";

// http://php.net/manual/en/function.str-ireplace.php
$string = "Michael a zis S*** si f***!";
echo str_ireplace(array("s***","f***"),"cenzurat",$string)."<br>";

$string = "Ana are mere!";
// http://php.net/manual/en/function.strpos.php
echo strpos($string,"mere")."<br>";
echo strpos($string,"Ana")."<br>";
var_dump(strpos($string,"Alexandra")); echo "<br>";
if(strpos($string,"Ana")!==false){
	echo "FOUND!<br>";
}

// http://php.net/manual/en/function.explode.php
$array = explode(" ",$string);
var_dump($array); echo "<br>";

$array = array("Ene","are","carti");
echo implode(" ",$array);

$currency = 4.5824 - 0.00102;
$currency *= 1.582/1.439;
echo number_format($currency,2)."<br>";
echo number_format($currency,4)."<br>";

$euro = 4.5915;
echo $euro."<br>";
echo number_format($euro+$euro*63/10000,2)."00<br>";
echo number_format($euro-$euro*67/10000,2)."<br>";

$paragraf = "Ana are mere. Ene are carti.";
var_dump(explode(".",$paragraf));