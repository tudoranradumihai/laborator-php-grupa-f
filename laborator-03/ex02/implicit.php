<?php

function something($parameter = "default"){
	echo $parameter."<br>";
}

something("something");
something(NULL);
something();

function f1($p1="default1",$p2="default2"){
	echo "p1:$p1<br>";
	echo "p2:$p2<br>";
}

f1(1,2);
f1(1);
f1(NULL,2);
f1();