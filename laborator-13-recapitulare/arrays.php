<?php

$names = array("Radu","Patricia","Darius","Mihai","Marian");
$fruits = array("Mere","Pere","Prune","Caise","Pomello");

/*

// V1 Clean Version

shuffle($names);
shuffle($fruits);

$array = array_combine($names, $fruits);
foreach($array as $key => $value){
	echo "$key mananca $value<br>";
}
*/

// V2 Without array_combine, shuffle

while(count($names)>0){
	$k1 = rand(0,count($names)-1);
	$k2 = rand(0,count($fruits)-1);
	echo $names[$k1]." mananca ".$fruits[$k2]."<br>"; 
	unset($names[$k1]);
	unset($fruits[$k2]);
	$names = array_values($names);
	$fruits = array_values($fruits);
}
