<?php

/*
1) trebuie latitudinea sa se incadreze in -180 +180
2) trebuile longitudinea se se incadreze in -90 +90

- daca nu se incadreaza in parametrii astia, atunci zice ca coordonatele nu sunt specificate corect
- daca se incadreaza, vreau sa imi spuneti in ce sector de timp GMT se afla si sa imi spuneti daca ii in emisfera nordica sau sudica, respectiv estica sau vestica

1. paris
2. yohanesburg
3. Talkeetna
4. Matancilla
*/

function ex01($latitude,$longitude){
	if($latitude>-90 && $latitude<90 && $longitude>-180 && $longitude<180){
		$string = "";
		if($longitude>0){
			$string .= "N";
		} else {
			$string .= "S";
		}
		if($latitude>0){
			$string .= "E";
		} else {
			$string .= "V";
		}
		
		$GMT = ceil($latitude/15);
		echo $string." GMT$GMT";
	} else {
		echo "Coordinates are incorrect.";
	}
}

ex01(-71.0534548, -31.4563992);