<form method="POST" action="create.php">
	<div>
		<label for="name">Name</label><br>
		<input type="text" name="name" id="name" placeholder="Product Name">
	</div>
	<div>
		<label for="description">Description</label><br>
		<textarea name="description" id="description"></textarea>
	</div>
	<div>
		<label for="price">Price</label><br>
		<input type="text" name="price" id="price" placeholder="999.99">
	</div>
	<div>
		<label for="stock">Stock</label><br>
		<input type="text" name="stock" id="stock" placeholder="0">
	</div>
	<div>
		<input type="submit">
	</div>
</form>