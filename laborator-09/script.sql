CREATE DATABASE grupa_f;

# CREATE TABLE IF NOT EXISTS
CREATE TABLE users (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(255),
	lastname VARCHAR(255),
	email_address VARCHAR(255) UNIQUE,
	password VARCHAR(255)
);
DROP TABLE users;
DROP TABLE IF EXISTS users;

INSERT INTO users 
	(firstname,lastname,email_address,password)
	VALUES
	("John","Doe","john.doe@email.com","1234");

INSERT INTO users 
	(firstname,lastname,email_address,password)
	VALUES
	("Jane","Doe","jane.doe@email.com","1234"),
	("Jack","Doe","jack.doe@email.com","1234");

SELECT * FROM users;
SELECT firstname, lastname FROM users;
SELECT * FROM users WHERE id=3;
SELECT * FROM users WHERE id=2 OR id=3;
SELECT * FROM users WHERE email_address LIKE "jack.doe@email.com";
SELECT * FROM users WHERE email_address LIKE "%email.com";
SELECT * FROM users WHERE email_address LIKE "jane%";
SELECT * FROM users WHERE email_address LIKE "%doe%";

CREATE TABLE products(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	description TEXT,
	price FLOAT(11.2) DEFAULT 0,
	stock INT(11) DEFAULT 0,
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO products (name,description,price,stock)
	VALUES
	("iPhone 7 Plus","Telefon Auriu",899.90,5),
	("Samsung Galaxy S8","Telefon Argintiu",699.90,10);

UPDATE products
SET description = "Telefon Auriu Mare",
	price = 919.90
WHERE id=1;

DELETE FROM products WHERE id=2;