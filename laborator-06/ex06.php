<?php

$dates = array();
$temporaryDate = strtotime("last Friday");
array_push($dates,$temporaryDate);
$counter = 4;
while($counter>0){
	$temporaryDate = strtotime(date("d.m.Y",$temporaryDate)." - 7 days");
	array_push($dates,$temporaryDate);
	$counter--;
}

foreach ($dates as $date){
	$link = "http://api.fixer.io/".date("Y-m-d",$date);
	$content = file_get_contents($link);
	$content = json_decode($content,TRUE);
	echo $date." - ".$content["rates"]["RON"]."<br>";
}

/* HOMEWORK: Afisati variatia cu + si - a cursului valutar in format de 4 zecimale */