<?php

// declarare array gol
$a = array();
// declarare array gol 
$b = [];
// declarare array cu valori fara chei
$c = array(1,2,3,4,5); 
// declarare array cu valori fara chei
$d = array("Radu","Tudoran");
// adaugare valoare fara a specifica cheia, cheia primita o sa fie urmatoare cheie numerica disponibila
$d[] = "Mihai";
// adaugare valoare cu cheie numerica
$d[3] = "Vasile";
$d[10] = "Petronela";
$d[] = "Bertrand";

$e = array();
$e["firstname"] = "Radu";
$e[] = "Tudoran";
var_dump($e);

$f = array(0=>"Radu",1=>"Altceva","text"=>"Whatever");
$f["ceva"] = "ceva";
$f = array(2=>"Mihai");
var_dump($f);