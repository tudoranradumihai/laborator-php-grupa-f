<?php

// NULL
$a = NULL;

// BOOLEAN
$b = true;
var_dump($b);echo "<br>";
$c = false;
var_dump($c);echo "<br>";

// INTEGER
$d = 1;
var_dump($d);echo "<br>";

// FLOAT
$e = 1.5;
var_dump($e);echo "<br>";
$f = 1.5e3;
var_dump($f);echo "<br>";

// STRING
$g = "Hello World!";
var_dump($g);echo "<br>";
$h = "";
var_dump($h);echo "<br>";

// STRING INCLUSION
$s1 = "Hello";
$s2 = "World";
echo $s1." ".$s2."<br>";
echo "$s1 $s2<br>";
echo '$s1 $s2<br>';
