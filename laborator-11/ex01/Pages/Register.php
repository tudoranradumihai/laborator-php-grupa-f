<h1>Register</h1>
<form id="register">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="firstname">Firstname</label>
      <input type="text" class="form-control" id="firstname" name="firstname" placeholder="John">
    </div>
    <div class="form-group col-md-6">
      <label for="lastname">Lastname</label>
      <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Doe">
    </div>
    <div class="form-group col-md-12">
      <label for="email">Email Address</label>
      <input type="email" class="form-control" id="email" name="email" placeholder="john.doe@domain.com">
    </div>
    <div class="form-group col-md-6">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="">
    </div>
    <div class="form-group col-md-6">
      <label for="password2">Confirm Password</label>
      <input type="text" class="form-control" id="password2" name="password2" placeholder="">
    </div>
  </div>
  <button type="submit" class="btn btn-primary" style="background-color: #563d7c;">Register</button>
</form>