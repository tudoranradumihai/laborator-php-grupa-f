<form>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="firstname">Firstname</label>
      <input type="text" class="form-control" id="firstname" placeholder="John">
    </div>
    <div class="form-group col-md-6">
      <label for="lastname">Lastname</label>
      <input type="text" class="form-control" id="lastname" placeholder="Doe">
    </div>
    <div class="form-group col-md-6">
      <label for="email">Email Address</label>
      <input type="email" class="form-control" id="email" placeholder="john.doe@domain.com">
    </div>
    <div class="form-group col-md-6">
      <label for="phone">Phone</label>
      <input type="text" class="form-control" id="phone" placeholder="0700000000">
    </div>
  </div>
  <button type="submit" class="btn btn-primary" style="background-color: #563d7c;">Sign in</button>
</form>