<DOCTYPE html>
<html>
	<head>
		<?php require "Partials/HeaderResources.php"; ?>
	</head>
	<body>
		<div class="container">
			<?php require "Partials/Header.php"; ?>
			<main>
			<?php
			if(array_key_exists("page", $_GET)){	
				$filepath = "Pages/".$_GET["page"].".php";
				if(file_exists($filepath)){
					require $filepath;
				} else {
					header("Location: index.php");
				}
			} else {
				require "Pages/Homepage.php";
			}
			?>
			</main>
			<?php require "Partials/Footer.php"; ?>
		</div>
		<?php require "Partials/FooterResources.php"; ?>
	</body>
</html>