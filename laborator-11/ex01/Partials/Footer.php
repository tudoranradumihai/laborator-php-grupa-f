<!-- FOOTER BEGIN -->
<footer>
	<div class="row">
		<div class="col-md-3">
			<img class="img-fluid " src="https://getbootstrap.com/assets/img/bootstrap-stack.png">
		</div>
		<div class="col-md-3">
			<div class="footer-menu">			
				<a href="index.php">Homepage</a><br>
				<a href="index.php?page=Products">Products</a><br>
				<a href="index.php?page=Contact">Contact</a>
			</div>
		</div>
		<div class="col-md-6">
			<div class="footer-description">			
				Lorem ipsum dolor sit amet, nobis postea electram ad his, sed ipsum propriae te. Eu his vivendo tibique appellantur, nobis scriptorem vim an, his id nullam accusata scripserit. Usu et omnes dictas mentitum, tamquam saperet democritum ut mei. Harum possim tincidunt id ius. No cum doming offendit. Nonumy necessitatibus sit ad, modus malorum detracto vim no, munere oporteat ex eum. Sea inani omnesque tincidunt eu, ea nec integre scripserit comprehensam, per eu verear interpretaris.
			</div>
		</div>
	</div>
</footer>
<!-- FOOTER END -->