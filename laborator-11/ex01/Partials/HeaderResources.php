<!-- HEADER RESOURCES BEGIN -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<style>
	header, nav, footer {
		margin-top: 20px;
		margin-bottom: 20px;
	} 
	a {
		color: #563d7c;
	}
	footer {
		color: #FFFFFF;
		background-color: #563d7c;
	}
	footer a {
		color: #FFFFFF;
	}
	header img, footer img {
		padding: 50px;
	}
	.footer-menu, .footer-description {
		padding: 25px;
	}
</style>
<!-- HEADER RESOURCES END -->