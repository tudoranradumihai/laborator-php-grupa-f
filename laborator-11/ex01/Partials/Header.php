<!-- HEADER BEGIN -->
<header>
	<div class="row">
		<div class="col-md-3">
			<img class="img-fluid" src="https://getbootstrap.com/assets/img/bootstrap-stack.png">
		</div>
		<div class="col-md-9 text-right">
			<a href="index.php?page=Login">Login</a> |
			<a href="index.php?page=Register">Register</a>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #563d7c;">
		<a class="navbar-brand" href="#">Bootstrap Tutorial</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="true" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="index.php">Homepage</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="index.php?page=Products">Products</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="index.php?page=Contact">Contact</a>
				</li>
			</ul>
		</div>
	</nav>
</header>
<!-- HEADER END -->