<?php
session_start();
function generateInputField($type,$name,$label,$placeholder,$class=""){
	$value = "";
	if(array_key_exists("user", $_SESSION)){
		if(array_key_exists($name, $_SESSION["user"])){
			$value = $_SESSION["user"][$name];
			unset($_SESSION["user"][$name]);
		}
	}
	$HTML  = "<div class=\"form-group $class\">"."\n";
	$HTML .= "<label for=\"$name\">$label</label>"."\n";
	$HTML .= "<input type=\"$type\" class=\"form-control\" id=\"$name\" name=\"$name\" placeholder=\"$placeholder\" value=\"$value\">"."\n";
	$HTML .= "</div>"."\n";
	echo $HTML;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<form method="POST" action="create.php">
				<h1>New User</h1>
				<?php
				if(array_key_exists("errors", $_SESSION)){
					foreach($_SESSION["errors"] as $error){
						echo "<div class=\"alert alert-danger\" role=\"alert\">$error</div>";
					}
					unset($_SESSION["errors"]);
				}
				?>
				<div class="form-row">
					<?php
						generateInputField("text","firstname","Firstname","John","col-md-6");
						generateInputField("text","lastname","Lastname","Doe","col-md-6");
						generateInputField("text","pin","Personal Identification Number","1YYMMDD000000","col-md-6");
						generateInputField("date","birthdate","Birthdate","MM/DD/YYYY","col-md-6");
						generateInputField("text","address","Address","","col-md-12");
						generateInputField("text","city","City","","col-md-6");
						generateInputField("text","city","Zip","","col-md-6");
						generateInputField("text","country","Country","","col-md-6");
						generateInputField("email","email","Email Address","","col-md-12");
						generateInputField("password","password","Password","","col-md-6");
						generateInputField("password","password2","Confirm Password","","col-md-6");
					?>
				</div>
				<button type="submit" class="btn btn-primary">Create User</button>
			</form>
		</div>
	</body>
</html>
<?php
if(array_key_exists("user", $_SESSION)){
	unset($_SESSION["user"]);
}
?>