<?php
session_start();
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$validation = true;
	$errors = array();
	$user = array();

	$requiredFields = array("firstname","lastname","pin","birthdate","address","city","country","email");
	foreach($requiredFields as $field){
		if(empty(trim($_POST[$field]))){
			$validation = false;
			array_push($errors, "Field '$field' is required");
		} else {
			$user[$field] = $_POST[$field];
		}
	}


	if($validation){
		/* ADAUGARE IN DB */
	} else {
		$_SESSION["errors"] = $errors;
		$_SESSION["user"] = $user;
		header("Location: new.php");
	}

} else {
	header("Location: new.php");
}