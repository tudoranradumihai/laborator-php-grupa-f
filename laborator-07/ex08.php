// https://www.w3schools.com/html/html_forms.asp
<form>
	Text:<br>
	<input type="text"><br>
	Parola:<br>
	<input type="password"><br>
	Email:<br>
	<input type="email"><br>
	Select:<br>
	<select>
		<option value="0">Zero</option>
		<option value="1" selected>Unu</option>
	</select><br>
	MultiSelect:<br>
	<select multiple>
		<option value="0">Zero</option>
		<option value="1" selected>Unu</option>
	</select><br>
	Textarea:<br>
	<textarea></textarea><br>
	Radio:<br>
	<input type="radio" name="gender" value="1">M<br>
	<input type="radio" name="gender" value="2">F<br>
	Checkbox:<br>
	<input type="checkbox" name="preferences" value="1">M<br>
	<input type="checkbox" name="preferences" value="2">F<br>
	<input type="date">
	<input type="reset">
	<input type="submit">
</form>