<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$url = "http://api.fixer.io/".$_POST["date"];
	if(!file_exists("history")){
		mkdir("history");
	}
	$content = file_get_contents($url);
	$content = json_decode($content,TRUE);
	$file = fopen("history/".$_POST["date"].".txt","w");
	foreach($content["rates"] as $key => $value){
		$array = array($key=>$value);
		fputs($file,json_encode($array).PHP_EOL);
	}
	fclose($file);
}
?>
<form method="POST">
	<input type="date" name="date" />
	<input type="submit" />
</form>